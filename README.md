# MT15-2021

**1. Créer un compte Gitlab**

Rendez-vous sur https://gitlab.com.

Créez un compte en cliquant sur "Register now" en prenant de choisir un mot de passe que vous retiendrez facilement, car vous en aurez besoin.

Ne PAS créer un compte en se connectant avec google par exemple, cela empêche les sauvegardes!!!

**2. Forker le projet contenant les tps**

Connectez-vous sur votre compte gitlab, puis cliquez sur le lien:

https://gitlab.com/yasmineUTT/tp_1

Cliquez sur "Fork" en haut à droite 


Cliquez sur le namespace qui correspond à votre nom.

Vous avez récupéré le projet contenant les tp sur votre compte gitlab.

Copiez l'url de la page qui s'affiche. **Ce n'est PAS** https://gitlab.com/yasmineUTT/tp_1

L'url doit être https://gitlab.com/###votre_pseudo###/tp_1/

**3. Lancer le projet sur binder**

Cliquez sur https://mybinder.org/

Selectionnez "GitLab.com" à la place "GitHub" dans la premier menu

Puis coller l'url copiée précédemment dans la première ligne du formulaire.

Cliquez enfin sur "launch".

Après quelques minutes, l'environnement de travail doit être chargé et prêt à l'emploi.


**4. Sauvegarder les modifications**


Toutes les modifications que vous allez faire seront perdues si on ne les transmet pas au projet gitlab.

Pour éviter cela, nous allons préparer l'envoi des modifications effectuées sur votre projet gitlab.

Cliquez sur **+** en haut à droite, puis **"Terminal"**

Une fois dans le terminal il faut taper en remplaçant par les bonnes valeurs:

```shell
git config --global user.email "mail@example.com"

git config --global user.name "mon_nom"
```
Une fois que vous aurez effectuer des modifications et que vous les aurez enregistrées ( je vous conseille de le faire régulièrement), revenez sur le terminal et tapez:

```shell
git add .
git commit -am "message"
git push
```

Le terminal vous demandera vos identifiants GitLab.

Ainsi les modifications sont envoyées à votre projet gitlab. Donc même en cas de deconnexion, il suffira de reprendre la procédure à partir de **"lancer le projet sur binder"**

Si jamais vous voulez ajouter des fichiers particuliers, il faudra d'abord taper :

`git add le_nom_du_fichier`

Avant de taper le **git commit**
